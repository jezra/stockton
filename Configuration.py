'''
this is a part of the stockton project
copyright 2016 jezra lickter http://www.jezra.net

This software is released under the GPLv3 license
https://opensource.org/licenses/gpl-3.0.html
'''
import configparser
import os, stat
class Configuration:
  def __init__(self, app_name):
    #define the config dir and file
    self.config_dir = os.path.join(os.path.expanduser("~"),'.config', app_name )
    self.config_file = os.path.join(self.config_dir, app_name+".conf" )
    #make the config dir if not exists
    if not os.path.exists(self.config_dir):
      os.makedirs(self.config_dir)
    self.config = configparser.RawConfigParser()

    if os.path.exists( self.config_file ):
      try:
        self.config.read(self.config_file)
      except:
        print("Could not read "+ self.config_file )

  def save(self):
    f = open(self.config_file,"w")
    self.config.write(f)
    f.close()
    #set the permissions to read/write owner only //yes, everytime
    os.chmod(self.config_file, stat.S_IWRITE | stat.S_IREAD)

  def set(self,section, key, value):
    if not self.config.has_section(section) :
      self.config.add_section(section)
    self.config.set(section,key,value)

  def get(self, section, key, default=None):
    try:
      val = self.config.get(section,key)
    except :
      print( "key exceptions %s : %s" % (section, key) )
      print("Using default: ",default)
      val = default
    return val

  def get_int(self, section, key, default=None):
    val = self.get(section, key, default)
    if val != None:
      val = int(val)
    return val

  def get_bool(self, section, option, default=None):
    try:
      val = self.config.get(section, option)
    except:
      val = False

    if val == 'True':
      val = True
    elif val == 'False':
      val = False
    else:
      if default != None:
        val = default
      else:
        val = False
    return val

