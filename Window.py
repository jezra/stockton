'''
this is a part of the stockton project
copyright 2016 jezra lickter http://www.jezra.net

This software is released under the GPLv3 license
https://opensource.org/licenses/gpl-3.0.html
'''
#import what is needed
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
#get webkit
gi.require_version('WebKit2', '4.0')
from gi.repository import WebKit2 as WebKit
from gi.repository import GObject
from gi.repository import GdkPixbuf
from gi.repository import Gdk

class Window(Gtk.Window):
  #what signals will we emit?
  __gsignals__ = {
  'save_state': (GObject.SIGNAL_RUN_FIRST, None, (str,) )
  }

  #default instance variables
  url = ""
  input_hidden = True

  def __init__(self, config_dir, app_name, app_version):
    #init the parent class of this instance
    Gtk.Window.__init__(self, title="Stockton", type=Gtk.WindowType.TOPLEVEL )

    # Create input widgets
    self.edit = Gtk.Entry.new()
    self.edit.set_placeholder_text("Enter URL")
    self.button = Gtk.Button.new_with_label("Go")
    #connection actions to input signals
    # Add button signal to "go" slot
    self.button.connect('clicked', self.set_url)
    # add a signal to the line edit return pressed
    self.edit.connect('activate',self.set_url)

    #create a horizontal layout for the input
    self.input_hbox = Gtk.HBox(False)

    self.input_hbox.pack_start(self.edit, True, True, 0)
    self.input_hbox.pack_end(self.button, False, False, 0)

    #make a settings thingy for the webview we are going to create
    settings = WebKit.Settings.new()
    ''' set some properties of the settings thingy '''
    #local storage
    settings.set_enable_html5_local_storage(True)

    settings.set_allow_universal_access_from_file_urls(True)
    #enable developerextras; debugging HTML/CSS/JS is a FEATURE!!!
    settings.set_enable_developer_extras(True)

    #create a custom user agent
    agent_str = settings.get_user_agent()
    settings.set_property("user-agent", agent_str+" "+app_name.capitalize()+"/"+app_version)

    #create a web rendering widget
    self.web = WebKit.WebView.new()
    #set the settings
    self.web.set_settings(settings)


    #### connect some signals to the webview
 
    # callback when page load is complete
    self.web.connect('load_changed', self.cb_load_changed)
    
    #### add a bunch of code in order to detect a favicon change
    # get the context
    self.context = self.web.get_context()
    #use the default directory for storing the favicon db
    self.context.set_favicon_database_directory(None)
    #get the favicon database
    self.db = self.context.get_favicon_database()
    #connect the favicon-changed signal .... sheesh, I liked webkit1 better for this :)
    self.db.connect('favicon-changed', self.cb_favicon_changed)
    
    # Create a vertical layout and add widgets
    scrolled_window = Gtk.ScrolledWindow.new()
    scrolled_window.add(self.web)
    vbox = Gtk.VBox(False)
    vbox.pack_start(scrolled_window, True, True, 0)
    vbox.pack_end(self.input_hbox, False, False,0)

    self.add(vbox)

    #create an acccelerator group
    accel = Gtk.AccelGroup.new()

    #make a ctrl+q quit
    accel.connect(Gdk.keyval_from_name("q"),
      Gdk.ModifierType.CONTROL_MASK,
      Gtk.AccelFlags.VISIBLE, self.emit_quit )

    #make an accelerator to toggle input visibility
    accel.connect(Gdk.keyval_from_name("g"),
       Gdk.ModifierType.CONTROL_MASK,
       Gtk.AccelFlags.VISIBLE,
       self.toggle_input )

    #refresh
    accel.connect(Gdk.keyval_from_name("r"),
       Gdk.ModifierType.CONTROL_MASK,
       Gtk.AccelFlags.VISIBLE,
       self.reload_page )

    #toggle window decoration
    accel.connect(Gdk.keyval_from_name("w"),
       Gdk.ModifierType.CONTROL_MASK,
       Gtk.AccelFlags.VISIBLE,
       self.toggle_decoration )

    #save the current state
    accel.connect(Gdk.keyval_from_name("s"),
       Gdk.ModifierType.CONTROL_MASK,
       Gtk.AccelFlags.VISIBLE,
       self.emit_save_state )

    #show and select all in input
    accel.connect(Gdk.keyval_from_name("l"),
       Gdk.ModifierType.CONTROL_MASK,
       Gtk.AccelFlags.VISIBLE,
       self.focus_url_input )

    accel.lock()
    #attach the accel group to the widow
    self.add_accel_group(accel)

  ''' accelerator callbacks '''
  def emit_quit(self, accel_group, acceleratable, keyval, modifier):
    self.emit("delete-event", None)

  def emit_save_state(self, accel_group, acceleratable, keyval, modifier):
    self.emit("save_state", self.url)

  def reload_page(self, accel_group, acceleratable, keyval, modifier):
    self.web.reload()
    return True

  def toggle_decoration(self, accel_group, acceleratable, keyval, modifier):
    self.set_decorated(not self.get_decorated())
    return True

  def toggle_input(self, accel_group, acceleratable, keyval, modifier):
    if self.input_hidden:
      self.input_hbox.show()
      self.input_hidden = False
    else:
      self.input_hbox.hide()
      self.input_hidden = True
    return True

  def focus_url_input(self, accel_group, acceleratable, keyval, modifier):
    #display the input
    self.input_hbox.show()
    self.input_hidden = False
    #select all of the input
    self.edit.grab_focus()

  def hide_input(self):
    self.input_hbox.hide()

  def update_window_icon(self):
    #get the icon 
    icon = self.web.get_favicon()
    # write the icon to a temp png
    temp_file = "/tmp/icon.png"
    icon.write_to_png(temp_file)
   
    #get a pixbuf from the temp_file
    pixbuf = GdkPixbuf.Pixbuf.new_from_file(temp_file)
    #set the icon to the pixbuf
    self.set_icon(pixbuf)

  def set_url(self, trigger=None, url=None):
    if url==None:
      url = self.edit.get_text()
    else:
      self.edit.set_text(url)
    #does the url start with http://?
    if not url.startswith("http") and not url.startswith("file://"):
      url = "http://"+url
    self.url = url
    self.web.load_uri(url)

  def inspector_show(self, inspector, webview):
    #create the inspector webview that this method will return
    inspector_view = WebKit.WebView()
    #create a window for the inspector
    self.inspector_window = Gtk.Window.new(Gtk.WindowType.TOPLEVEL)
    #create a scrolledwindow to hold the webview
    sw = Gtk.ScrolledWindow.new()
    sw.add(inspector_view)
    self.inspector_window.add(sw)
    self.inspector_window.show_all()
    #determine the title for the inspector
    title = self.web.get_title() + " Inspector"
    self.inspector_window.set_title(title)
    #set the inspector's icon
    self.inspector_window.set_icon(self.get_icon())
    return inspector_view

  def cb_load_changed(self, view, event):
    if event == WebKit.LoadEvent.FINISHED:
      #try to check the title
      title = view.get_title()
      #is there an actual title?
      if title:
        self.set_title(title)
      #update the favicon
      self.update_window_icon()

  def cb_favicon_changed(self, db, page_uri, favicon_uri):
    #favicon has changed, update the window icon 
    self.update_window_icon()

