#!/usr/bin/env python3

'''
this is a part of the stockton project
copyright 2016 jezra lickter http://www.jezra.net

This software is released under the GPLv3 license
https://opensource.org/licenses/gpl-3.0.html
'''

import signal, sys, os
import gi

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

from Window import Window
from Configuration import Configuration

class StocktonController:
  def __init__(self, args):
    #name ourselves
    self.app_name = "stockton";
    self.app_version = "0.2.0"
    #get some configuration data
    self.config = Configuration(self.app_name)
    #build the UI
    self.window = Window(self.config.config_dir, self.app_name, self.app_version)
    self.window.show_all()

    #connect the windows signal
    self.window.connect('save_state', self.save_state )
    self.window.connect("delete-event", self.quit)

    #if we received a command line arg, it might be a URL
    if len(args) > 1:
      self.window.set_url( url=args[1] )
    else:
      #get the readme text
      cur_dir = os.path.dirname( os.path.realpath( __file__ ) )
      readme_file = os.path.join(cur_dir, "README.md")
      text = open(readme_file,'r').read()
      self.window.web.load_plain_text(text)
    #show the window and all of its widgets
    self.set_window()

  def save_state(self, window, url):
    #get the coordinates of the window
    (l,t) = self.window.get_position()
    (w,h) = self.window.get_size()
    d =  self.window.get_decorated()
    self.config.set( url, "decorated", d)
    self.config.set( url, "width", w)
    self.config.set( url, "height", h)
    self.config.set( url, "top", t)
    self.config.set( url, "left", l)
    self.config.save()


  def decorate_window(self, value):
    self.window.set_decorated(value)

  def set_window(self):
    #what is the url of the window??
    url = self.window.url
    #try to read a config for this
    h = self.config.get_int(url, 'height', "600")
    w = self.config.get_int(url, 'width', "800")
    t = self.config.get_int(url, 'top',10)
    l = self.config.get_int(url, 'left',10)
    d = self.config.get_bool(url, 'decorated', True)

    self.window.move(l,t)
    self.window.resize(w,h)
    #set the decoration
    self.decorate_window(d)
    #hide the input!
    self.window.hide_input()

  def quit(self, window, arg1=None):
    #so long, buddy!
    Gtk.main_quit()

  def run(self):
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    #start the app running
    Gtk.main()

if __name__ == '__main__':
  #make a stockton controller
  sc = StocktonController(sys.argv)
  #run the stockton controller
  sc.run()
