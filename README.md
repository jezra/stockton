Stockton
=====

Stockton is a simple web application wrapper for a given URL, and allows a user to position and save the location of a web page on the user's monitor.

Examples
------------

### Web page in a Stockton window

A user likes to keep <http://googlebutter.com> open in a browser tab, but would prefer to have the webpage running as a standalone application with its own resizable window. The user would use Stockton in the following way:


1. run `./stockton.py googlebutter.com`
2. resize the window (perhaps use Ctrl+w to toggle the window frame)
3. use Ctrl+s to save the state of the stockton window

The next time the user runs `./stockton.py googlebutter.com`, the Stockton window will be in the same location and state

### Local HTML5 app in a Stockton window

A user has a local directory containing javascript, html, css, etc. and the user would like to run the code as a 'desktop' app.

1. run `./stockton.py file:///path/to/index.html`
2. resize the window (perhaps use Ctrl+w to toggle the window frame)
3. use Ctrl+s to save the state of the stockton window

The next time the user runs `./stockton.py file:///path/to/index.html`, the Stockton window will be in the same location and state

The user can view a debug/inspector window by right-clicking on an html element in the Stockton window and selecting "Inspect Element" from the context menu.

Controls
----------

* ctrl+s : save window state
* ctrl+r : refresh current URL
* ctrl+q : quit
* ctrl+w : toggle window decoration
* ctrl+g : toggle display of URL input
* ctrl+l :  show URL input and select content of entry field



What's crackin, buddies? :)
